// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3.2 (swiftlang-1200.0.45 clang-1200.0.32.28)
// swift-module-flags: -target armv7-apple-ios9.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name flutter_inappwebview
import AVFoundation
import Flutter
import Foundation
import OrderedSet
import SafariServices
import Swift
import UIKit
import WebKit
@_exported import flutter_inappwebview
@objc @_hasMissingDesignatedInitializers public class ChromeSafariBrowserManager : ObjectiveC.NSObject, Flutter.FlutterPlugin {
  @objc public static func register(with registrar: Flutter.FlutterPluginRegistrar)
  @objc public func handle(_ call: Flutter.FlutterMethodCall, result: @escaping Flutter.FlutterResult)
  public func open(id: Swift.String, url: Swift.String, options: [Swift.String : Any?], menuItemList: [[Swift.String : Any]], result: @escaping Flutter.FlutterResult)
  @objc deinit
  @objc override dynamic public init()
}
@objc @_inheritsConvenienceInitializers public class FlutterMethodCallDelegate : ObjectiveC.NSObject {
  @objc override dynamic public init()
  public func handle(_ call: Flutter.FlutterMethodCall, result: @escaping Flutter.FlutterResult)
  @objc deinit
}
@objc @_hasMissingDesignatedInitializers public class FlutterWebViewController : ObjectiveC.NSObject, Flutter.FlutterPlatformView {
  @objc public func view() -> UIKit.UIView
  public func makeInitialLoad(params: Foundation.NSDictionary)
  @objc deinit
  @objc override dynamic public init()
}
@objc @_hasMissingDesignatedInitializers public class FlutterWebViewFactory : ObjectiveC.NSObject, Flutter.FlutterPlatformViewFactory {
  @objc public func createArgsCodec() -> Flutter.FlutterMessageCodec & ObjectiveC.NSObjectProtocol
  @objc public func create(withFrame frame: CoreGraphics.CGRect, viewIdentifier viewId: Swift.Int64, arguments args: Any?) -> Flutter.FlutterPlatformView
  @objc deinit
  @objc override dynamic public init()
}
@objc public class HeadlessInAppWebView : flutter_inappwebview.FlutterMethodCallDelegate {
  public init(id: Swift.String, flutterWebView: flutter_inappwebview.FlutterWebViewController)
  override public func handle(_ call: Flutter.FlutterMethodCall, result: @escaping Flutter.FlutterResult)
  public func onWebViewCreated()
  public func prepare(params: Foundation.NSDictionary)
  public func setSize(size: flutter_inappwebview.Size2D)
  public func getSize() -> flutter_inappwebview.Size2D?
  public func dispose()
  @objc deinit
  @objc override dynamic public init()
}
@objc @_hasMissingDesignatedInitializers public class HeadlessInAppWebViewManager : ObjectiveC.NSObject, Flutter.FlutterPlugin {
  @objc public static func register(with registrar: Flutter.FlutterPluginRegistrar)
  @objc public func handle(_ call: Flutter.FlutterMethodCall, result: @escaping Flutter.FlutterResult)
  public static func run(id: Swift.String, params: [Swift.String : Any?])
  @objc deinit
  @objc override dynamic public init()
}
public enum HitTestResultType : Swift.Int {
  case unknownType
  case phoneType
  case geoType
  case emailType
  case imageType
  case srcAnchorType
  case srcImageAnchorType
  case editTextType
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
@objc public class HitTestResult : ObjectiveC.NSObject {
  public init(type: flutter_inappwebview.HitTestResultType, extra: Swift.String?)
  public static func fromMap(map: [Swift.String : Any?]?) -> flutter_inappwebview.HitTestResult?
  public func toMap() -> [Swift.String : Any?]
  @objc deinit
  @objc override dynamic public init()
}
public protocol InAppBrowserDelegate {
  func didChangeTitle(title: Swift.String?)
  func didStartNavigation(url: Foundation.URL?)
  func didUpdateVisitedHistory(url: Foundation.URL?)
  func didFinishNavigation(url: Foundation.URL?)
  func didFailNavigation(url: Foundation.URL?, error: Swift.Error)
  func didChangeProgress(progress: Swift.Double)
}
@objc @_hasMissingDesignatedInitializers public class InAppBrowserManager : ObjectiveC.NSObject, Flutter.FlutterPlugin {
  @objc public static func register(with registrar: Flutter.FlutterPluginRegistrar)
  @objc public func handle(_ call: Flutter.FlutterMethodCall, result: @escaping Flutter.FlutterResult)
  public func prepareInAppBrowserWebViewController(options: [Swift.String : Any?]) -> flutter_inappwebview.InAppBrowserWebViewController
  public func open(arguments: Foundation.NSDictionary)
  public func presentViewController(webViewController: flutter_inappwebview.InAppBrowserWebViewController)
  public func openWithSystemBrowser(url: Swift.String, result: @escaping Flutter.FlutterResult)
  @objc deinit
  @objc override dynamic public init()
}
@objc @_inheritsConvenienceInitializers public class InAppBrowserNavigationController : UIKit.UINavigationController {
  @objc deinit
  @available(iOS 5.0, *)
  @objc override dynamic public init(navigationBarClass: Swift.AnyClass?, toolbarClass: Swift.AnyClass?)
  @objc override dynamic public init(rootViewController: UIKit.UIViewController)
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objcMembers public class InAppBrowserOptions : flutter_inappwebview.Options<flutter_inappwebview.InAppBrowserWebViewController> {
  @objc deinit
}
@objc @_inheritsConvenienceInitializers public class InAppBrowserWebViewController : UIKit.UIViewController, flutter_inappwebview.InAppBrowserDelegate, UIKit.UIScrollViewDelegate, WebKit.WKUIDelegate, UIKit.UISearchBarDelegate {
  @objc override dynamic public func loadView()
  @objc override dynamic public func viewDidLoad()
  public func initLoad()
  @objc deinit
  @objc override dynamic public func viewDidDisappear(_ animated: Swift.Bool)
  @objc override dynamic public func viewWillDisappear(_ animated: Swift.Bool)
  public func prepareNavigationControllerBeforeViewWillAppear()
  public func prepareWebView()
  public func didChangeTitle(title: Swift.String?)
  public func didStartNavigation(url: Foundation.URL?)
  public func didUpdateVisitedHistory(url: Foundation.URL?)
  public func didFinishNavigation(url: Foundation.URL?)
  public func didFailNavigation(url: Foundation.URL?, error: Swift.Error)
  public func didChangeProgress(progress: Swift.Double)
  @objc public func searchBarSearchButtonClicked(_ searchBar: UIKit.UISearchBar)
  public func show(completion: (() -> Swift.Void)? = nil)
  public func hide(completion: (() -> Swift.Void)? = nil)
  @objc public func reload()
  @objc public func share()
  public func close(completion: (() -> Swift.Void)? = nil)
  @objc public func close()
  @objc public func goBack()
  @objc public func goForward()
  @objc public func goBackOrForward(steps: Swift.Int)
  public func setOptions(newOptions: flutter_inappwebview.InAppBrowserOptions, newOptionsMap: [Swift.String : Any])
  public func getOptions() -> [Swift.String : Any?]?
  public func dispose()
  public func onBrowserCreated()
  public func onExit()
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
}
@objc @_hasMissingDesignatedInitializers public class InAppWebView : WebKit.WKWebView, UIKit.UIScrollViewDelegate, WebKit.WKUIDelegate, WebKit.WKNavigationDelegate, WebKit.WKScriptMessageHandler, UIKit.UIGestureRecognizerDelegate, flutter_inappwebview.PullToRefreshDelegate {
  @objc override dynamic public var frame: CoreGraphics.CGRect {
    @objc get
    @objc set
  }
  @objc required dynamic public init(coder aDecoder: Foundation.NSCoder)
  @objc public func gestureRecognizer(_ gestureRecognizer: UIKit.UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIKit.UIGestureRecognizer) -> Swift.Bool
  @objc override dynamic public func hitTest(_ point: CoreGraphics.CGPoint, with event: UIKit.UIEvent?) -> UIKit.UIView?
  @objc override dynamic public func canPerformAction(_ action: ObjectiveC.Selector, withSender sender: Any?) -> Swift.Bool
  public func prepare()
  public func prepareAndAddUserScripts()
  public static func preWKWebViewConfiguration(options: flutter_inappwebview.InAppWebViewOptions?) -> WebKit.WKWebViewConfiguration
  @objc override dynamic public func observeValue(forKeyPath keyPath: Swift.String?, of object: Any?, change: [Foundation.NSKeyValueChangeKey : Any]?, context: Swift.UnsafeMutableRawPointer?)
  public func initializeWindowIdJS()
  public func goBackOrForward(steps: Swift.Int)
  public func canGoBackOrForward(steps: Swift.Int) -> Swift.Bool
  @available(iOS 11.0, *)
  public func takeScreenshot(with: [Swift.String : Any?]?, completionHandler: @escaping (Foundation.Data?) -> Swift.Void)
  @available(iOS 14.0, *)
  public func createPdf(configuration: [Swift.String : Any?]?, completionHandler: @escaping (Foundation.Data?) -> Swift.Void)
  @available(iOS 14.0, *)
  public func createWebArchiveData(dataCompletionHandler: @escaping (Foundation.Data?) -> Swift.Void)
  @available(iOS 14.0, *)
  public func saveWebArchive(filePath: Swift.String, autoname: Swift.Bool, completionHandler: @escaping (Swift.String?) -> Swift.Void)
  public func loadUrl(urlRequest: Foundation.URLRequest, allowingReadAccessTo: Foundation.URL?)
  public func postUrl(url: Foundation.URL, postData: Foundation.Data)
  public func loadData(data: Swift.String, mimeType: Swift.String, encoding: Swift.String, baseUrl: Foundation.URL, allowingReadAccessTo: Foundation.URL?)
  public func loadFile(assetFilePath: Swift.String) throws
  public func enablePluginScriptAtRuntime(flagVariable: Swift.String, enable: Swift.Bool, pluginScript: flutter_inappwebview.PluginScript)
  public func clearCache()
  public func injectDeferredObject(source: Swift.String, withWrapper jsWrapper: Swift.String?, completionHandler: ((Any?) -> Swift.Void)? = nil)
  @available(iOS 14.0, *)
  public func injectDeferredObject(source: Swift.String, contentWorld: WebKit.WKContentWorld, withWrapper jsWrapper: Swift.String?, completionHandler: ((Any?) -> Swift.Void)? = nil)
  @objc override dynamic public func evaluateJavaScript(_ javaScriptString: Swift.String, completionHandler: ((Any?, Swift.Error?) -> Swift.Void)? = nil)
  @available(iOS 14.0, *)
  public func evaluateJavaScript(_ javaScript: Swift.String, frame: WebKit.WKFrameInfo? = nil, contentWorld: WebKit.WKContentWorld, completionHandler: ((Swift.Result<Any, Swift.Error>) -> Swift.Void)? = nil)
  public func evaluateJavascript(source: Swift.String, completionHandler: ((Any?) -> Swift.Void)? = nil)
  @available(iOS 14.0, *)
  public func evaluateJavascript(source: Swift.String, contentWorld: WebKit.WKContentWorld, completionHandler: ((Any?) -> Swift.Void)? = nil)
  @available(iOS 14.0, *)
  public func callAsyncJavaScript(_ functionBody: Swift.String, arguments: [Swift.String : Any] = [:], frame: WebKit.WKFrameInfo? = nil, contentWorld: WebKit.WKContentWorld, completionHandler: ((Swift.Result<Any, Swift.Error>) -> Swift.Void)? = nil)
  @available(iOS 14.0, *)
  public func callAsyncJavaScript(functionBody: Swift.String, arguments: [Swift.String : Any], contentWorld: WebKit.WKContentWorld, completionHandler: ((Any?) -> Swift.Void)? = nil)
  @available(iOS 10.3, *)
  public func callAsyncJavaScript(functionBody: Swift.String, arguments: [Swift.String : Any], completionHandler: ((Any?) -> Swift.Void)? = nil)
  public func injectJavascriptFileFromUrl(urlFile: Swift.String, scriptHtmlTagAttributes: [Swift.String : Any?]?)
  public func injectCSSCode(source: Swift.String)
  public func injectCSSFileFromUrl(urlFile: Swift.String, cssLinkHtmlTagAttributes: [Swift.String : Any?]?)
  public func getCopyBackForwardList() -> [Swift.String : Any]
  @available(iOS 13.0, *)
  @objc public func webView(_ webView: WebKit.WKWebView, decidePolicyFor navigationAction: WebKit.WKNavigationAction, preferences: WebKit.WKWebpagePreferences, decisionHandler: @escaping (WebKit.WKNavigationActionPolicy, WebKit.WKWebpagePreferences) -> Swift.Void)
  @objc public func webView(_ webView: WebKit.WKWebView, decidePolicyFor navigationAction: WebKit.WKNavigationAction, decisionHandler: @escaping (WebKit.WKNavigationActionPolicy) -> Swift.Void)
  @objc public func webView(_ webView: WebKit.WKWebView, decidePolicyFor navigationResponse: WebKit.WKNavigationResponse, decisionHandler: @escaping (WebKit.WKNavigationResponsePolicy) -> Swift.Void)
  @objc public func webView(_ webView: WebKit.WKWebView, didStartProvisionalNavigation navigation: WebKit.WKNavigation!)
  @objc public func webView(_ webView: WebKit.WKWebView, didFinish navigation: WebKit.WKNavigation!)
  @objc public func webView(_ view: WebKit.WKWebView, didFailProvisionalNavigation navigation: WebKit.WKNavigation!, withError error: Swift.Error)
  @objc public func webView(_ webView: WebKit.WKWebView, didFail navigation: WebKit.WKNavigation!, withError error: Swift.Error)
  @objc public func webView(_ webView: WebKit.WKWebView, didReceive challenge: Foundation.URLAuthenticationChallenge, completionHandler: @escaping (Foundation.URLSession.AuthChallengeDisposition, Foundation.URLCredential?) -> Swift.Void)
  @objc public func webView(_ webView: WebKit.WKWebView, runJavaScriptAlertPanelWithMessage message: Swift.String, initiatedByFrame frame: WebKit.WKFrameInfo, completionHandler: @escaping () -> Swift.Void)
  @objc public func webView(_ webView: WebKit.WKWebView, runJavaScriptConfirmPanelWithMessage message: Swift.String, initiatedByFrame frame: WebKit.WKFrameInfo, completionHandler: @escaping (Swift.Bool) -> Swift.Void)
  @objc public func webView(_ webView: WebKit.WKWebView, runJavaScriptTextInputPanelWithPrompt message: Swift.String, defaultText defaultValue: Swift.String?, initiatedByFrame frame: WebKit.WKFrameInfo, completionHandler: @escaping (Swift.String?) -> Swift.Void)
  public func onScrollChanged(startedByUser: Swift.Bool, oldContentOffset: CoreGraphics.CGPoint?)
  @objc public func scrollViewDidZoom(_ scrollView: UIKit.UIScrollView)
  @objc public func webView(_ webView: WebKit.WKWebView, createWebViewWith configuration: WebKit.WKWebViewConfiguration, for navigationAction: WebKit.WKNavigationAction, windowFeatures: WebKit.WKWindowFeatures) -> WebKit.WKWebView?
  @objc public func webView(_ webView: WebKit.WKWebView, authenticationChallenge challenge: Foundation.URLAuthenticationChallenge, shouldAllowDeprecatedTLS decisionHandler: @escaping (Swift.Bool) -> Swift.Void)
  @objc public func webViewDidClose(_ webView: WebKit.WKWebView)
  @objc public func webViewWebContentProcessDidTerminate(_ webView: WebKit.WKWebView)
  @objc public func webView(_ webView: WebKit.WKWebView, didCommit navigation: WebKit.WKNavigation!)
  @objc public func webView(_ webView: WebKit.WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WebKit.WKNavigation!)
  public func onLoadStart(url: Swift.String?)
  public func onLoadStop(url: Swift.String?)
  public func onLoadError(url: Swift.String?, error: Swift.Error)
  public func onLoadHttpError(url: Swift.String?, statusCode: Swift.Int, description: Swift.String)
  public func onProgressChanged(progress: Swift.Int)
  public func onFindResultReceived(activeMatchOrdinal: Swift.Int, numberOfMatches: Swift.Int, isDoneCounting: Swift.Bool)
  public func onScrollChanged(x: Swift.Int, y: Swift.Int)
  public func onZoomScaleChanged(newScale: Swift.Float, oldScale: Swift.Float)
  public func onOverScrolled(x: Swift.Int, y: Swift.Int, clampedX: Swift.Bool, clampedY: Swift.Bool)
  public func onDownloadStart(url: Swift.String)
  public func onLoadResourceCustomScheme(url: Swift.String, result: Flutter.FlutterResult?)
  public func shouldOverrideUrlLoading(navigationAction: WebKit.WKNavigationAction, result: Flutter.FlutterResult?)
  public func onNavigationResponse(navigationResponse: WebKit.WKNavigationResponse, result: Flutter.FlutterResult?)
  public func onReceivedHttpAuthRequest(challenge: Foundation.URLAuthenticationChallenge, result: Flutter.FlutterResult?)
  public func onReceivedServerTrustAuthRequest(challenge: Foundation.URLAuthenticationChallenge, result: Flutter.FlutterResult?)
  public func onReceivedClientCertRequest(challenge: Foundation.URLAuthenticationChallenge, result: Flutter.FlutterResult?)
  public func shouldAllowDeprecatedTLS(challenge: Foundation.URLAuthenticationChallenge, result: Flutter.FlutterResult?)
  public func onJsAlert(frame: WebKit.WKFrameInfo, message: Swift.String, result: Flutter.FlutterResult?)
  public func onJsConfirm(frame: WebKit.WKFrameInfo, message: Swift.String, result: Flutter.FlutterResult?)
  public func onJsPrompt(frame: WebKit.WKFrameInfo, message: Swift.String, defaultValue: Swift.String?, result: Flutter.FlutterResult?)
  public func onConsoleMessage(message: Swift.String, messageLevel: Swift.Int)
  public func onUpdateVisitedHistory(url: Swift.String?)
  public func onTitleChanged(title: Swift.String?)
  public func onLongPressHitTestResult(hitTestResult: flutter_inappwebview.HitTestResult)
  public func onCallJsHandler(handlerName: Swift.String, _callHandlerID: Swift.Int64, args: Swift.String)
  public func onWebContentProcessDidTerminate()
  public func onPageCommitVisible(url: Swift.String?)
  public func onDidReceiveServerRedirectForProvisionalNavigation()
  public func isVideoPlayerWindow(_ notificationObject: Swift.AnyObject?) -> Swift.Bool
  @objc public func userContentController(_ userContentController: WebKit.WKUserContentController, didReceive message: WebKit.WKScriptMessage)
  public func findAllAsync(find: Swift.String?, completionHandler: ((Any?, Swift.Error?) -> Swift.Void)?)
  public func findNext(forward: Swift.Bool, completionHandler: ((Any?, Swift.Error?) -> Swift.Void)?)
  public func clearMatches(completionHandler: ((Any?, Swift.Error?) -> Swift.Void)?)
  public func scrollTo(x: Swift.Int, y: Swift.Int, animated: Swift.Bool)
  public func scrollBy(x: Swift.Int, y: Swift.Int, animated: Swift.Bool)
  public func pauseTimers()
  public func resumeTimers()
  public func printCurrentPage(printCompletionHandler: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
  public func getContentHeight() -> Swift.Int64
  public func zoomBy(zoomFactor: Swift.Float, animated: Swift.Bool)
  public func getZoomScale() -> Swift.Float
  public func getSelectedText(completionHandler: @escaping (Any?, Swift.Error?) -> Swift.Void)
  public func getHitTestResult(completionHandler: @escaping (flutter_inappwebview.HitTestResult) -> Swift.Void)
  public func requestFocusNodeHref(completionHandler: @escaping ([Swift.String : Any?]?, Swift.Error?) -> Swift.Void)
  public func requestImageRef(completionHandler: @escaping ([Swift.String : Any?]?, Swift.Error?) -> Swift.Void)
  public func clearFocus()
  public func getCertificate() -> flutter_inappwebview.SslCertificate?
  public func isSecureContext(completionHandler: @escaping (Swift.Bool) -> Swift.Void)
  public func canScrollVertically() -> Swift.Bool
  public func canScrollHorizontally() -> Swift.Bool
  public func enablePullToRefresh()
  public func disablePullToRefresh()
  public func createWebMessageChannel(completionHandler: ((flutter_inappwebview.WebMessageChannel) -> Swift.Void)? = nil) -> flutter_inappwebview.WebMessageChannel
  public func postWebMessage(message: flutter_inappwebview.WebMessage, targetOrigin: Swift.String, completionHandler: ((Any?) -> Swift.Void)? = nil) throws
  public func addWebMessageListener(webMessageListener: flutter_inappwebview.WebMessageListener) throws
  public func disposeWebMessageChannels()
  public func dispose()
  @objc deinit
  @objc override dynamic public init(frame: CoreGraphics.CGRect, configuration: WebKit.WKWebViewConfiguration)
}
@objc @_hasMissingDesignatedInitializers public class InAppWebViewMethodHandler : flutter_inappwebview.FlutterMethodCallDelegate {
  override public func handle(_ call: Flutter.FlutterMethodCall, result: @escaping Flutter.FlutterResult)
  @objc deinit
  @objc override dynamic public init()
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objcMembers public class InAppWebViewOptions : flutter_inappwebview.Options<flutter_inappwebview.InAppWebView> {
  @objc deinit
}
@objc @_hasMissingDesignatedInitializers public class LeakAvoider : ObjectiveC.NSObject {
  public func handle(_ call: Flutter.FlutterMethodCall, result: @escaping Flutter.FlutterResult)
  @objc deinit
  @objc override dynamic public init()
}
extension NSAttributedString {
  public static func fromMap(map: [Swift.String : Any?]?) -> Foundation.NSAttributedString?
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objcMembers public class Options<T> : ObjectiveC.NSObject {
  @objc deinit
}
@objc public class PluginScript : flutter_inappwebview.UserScript {
  @objc override dynamic public init(source: Swift.String, injectionTime: WebKit.WKUserScriptInjectionTime, forMainFrameOnly: Swift.Bool)
  public init(groupName: Swift.String, source: Swift.String, injectionTime: WebKit.WKUserScriptInjectionTime, forMainFrameOnly: Swift.Bool, requiredInAllContentWorlds: Swift.Bool = false, messageHandlerNames: [Swift.String] = [])
  @available(iOS 14.0, *)
  @objc override dynamic public init(source: Swift.String, injectionTime: WebKit.WKUserScriptInjectionTime, forMainFrameOnly: Swift.Bool, in contentWorld: WebKit.WKContentWorld)
  @available(iOS 14.0, *)
  public init(source: Swift.String, injectionTime: WebKit.WKUserScriptInjectionTime, forMainFrameOnly: Swift.Bool, in contentWorld: WebKit.WKContentWorld, requiredInAllContentWorlds: Swift.Bool = false, messageHandlerNames: [Swift.String] = [])
  @available(iOS 14.0, *)
  public init(groupName: Swift.String, source: Swift.String, injectionTime: WebKit.WKUserScriptInjectionTime, forMainFrameOnly: Swift.Bool, in contentWorld: WebKit.WKContentWorld, requiredInAllContentWorlds: Swift.Bool = false, messageHandlerNames: [Swift.String] = [])
  public func copyAndSet(groupName: Swift.String? = nil, source: Swift.String? = nil, injectionTime: WebKit.WKUserScriptInjectionTime? = nil, forMainFrameOnly: Swift.Bool? = nil, requiredInAllContentWorlds: Swift.Bool? = nil, messageHandlerNames: [Swift.String]? = nil) -> flutter_inappwebview.PluginScript
  @available(iOS 14.0, *)
  public func copyAndSet(groupName: Swift.String? = nil, source: Swift.String? = nil, injectionTime: WebKit.WKUserScriptInjectionTime? = nil, forMainFrameOnly: Swift.Bool? = nil, contentWorld: WebKit.WKContentWorld? = nil, requiredInAllContentWorlds: Swift.Bool? = nil, messageHandlerNames: [Swift.String]? = nil) -> flutter_inappwebview.PluginScript
  override public init(groupName: Swift.String?, source: Swift.String, injectionTime: WebKit.WKUserScriptInjectionTime, forMainFrameOnly: Swift.Bool)
  @available(iOS 14.0, *)
  override public init(groupName: Swift.String?, source: Swift.String, injectionTime: WebKit.WKUserScriptInjectionTime, forMainFrameOnly: Swift.Bool, in contentWorld: WebKit.WKContentWorld)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class PluginScriptsUtil {
  public static let VAR_PLACEHOLDER_VALUE: Swift.String
  public static let VAR_FUNCTION_ARGUMENT_NAMES: Swift.String
  public static let VAR_FUNCTION_ARGUMENT_VALUES: Swift.String
  public static let VAR_FUNCTION_ARGUMENTS_OBJ: Swift.String
  public static let VAR_FUNCTION_BODY: Swift.String
  public static let VAR_RESULT_UUID: Swift.String
  public static let GET_SELECTED_TEXT_JS_SOURCE: Swift.String
  @objc deinit
}
@objc @_hasMissingDesignatedInitializers public class PullToRefreshControl : UIKit.UIRefreshControl, Flutter.FlutterPlugin {
  public init(channel: Flutter.FlutterMethodChannel?, options: flutter_inappwebview.PullToRefreshOptions?)
  @objc public static func register(with registrar: Flutter.FlutterPluginRegistrar)
  public func prepare()
  @objc public func handle(_ call: Flutter.FlutterMethodCall, result: @escaping Flutter.FlutterResult)
  public func onRefresh()
  @objc public func updateShouldCallOnRefresh()
  public func dispose()
  @objc deinit
  @objc override dynamic public init()
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
}
public protocol PullToRefreshDelegate {
  func enablePullToRefresh()
  func disablePullToRefresh()
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class PullToRefreshOptions : flutter_inappwebview.Options<flutter_inappwebview.PullToRefreshControl> {
  @objc deinit
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @available(iOS 9.0, *)
@objcMembers public class SafariBrowserOptions : flutter_inappwebview.Options<flutter_inappwebview.SafariViewController> {
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @available(iOS 9.0, *)
public class SafariViewController : SafariServices.SFSafariViewController, Flutter.FlutterPlugin, SafariServices.SFSafariViewControllerDelegate {
  @objc public static func register(with registrar: Flutter.FlutterPluginRegistrar)
  @objc deinit
  public func prepareMethodChannel()
  @objc public func handle(_ call: Flutter.FlutterMethodCall, result: @escaping Flutter.FlutterResult)
  @objc override dynamic public func viewWillAppear(_ animated: Swift.Bool)
  @objc override dynamic public func viewDidDisappear(_ animated: Swift.Bool)
  @objc public func safariViewControllerDidFinish(_ controller: SafariServices.SFSafariViewController)
  @objc public func safariViewController(_ controller: SafariServices.SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Swift.Bool)
  @objc public func safariViewController(_ controller: SafariServices.SFSafariViewController, activityItemsFor URL: Foundation.URL, title: Swift.String?) -> [UIKit.UIActivity]
  public func onChromeSafariBrowserOpened()
  public func onChromeSafariBrowserCompletedInitialLoad()
  public func onChromeSafariBrowserClosed()
  public func dispose()
  @available(iOS 11.0, *)
  @objc override dynamic public init(url URL: Foundation.URL, configuration: SafariServices.SFSafariViewController.Configuration)
  @available(iOS, introduced: 9.0, deprecated: 11.0)
  @objc override dynamic public init(url URL: Foundation.URL, entersReaderIfAvailable: Swift.Bool)
}
@objc public class Size2D : ObjectiveC.NSObject {
  public init(width: Swift.Double, height: Swift.Double)
  public static func fromMap(map: [Swift.String : Any?]?) -> flutter_inappwebview.Size2D?
  public func toMap() -> [Swift.String : Any?]
  @objc override dynamic public init()
  @objc deinit
}
@objc public class SslCertificate : ObjectiveC.NSObject {
  public init(x509Certificate: Foundation.Data)
  public func toMap() -> [Swift.String : Any?]
  @objc deinit
  @objc override dynamic public init()
}
@objc public class SslError : ObjectiveC.NSObject {
  public init(errorType: Security.SecTrustResultType?)
  public func toMap() -> [Swift.String : Any?]
  @objc deinit
  @objc override dynamic public init()
}
@objc public class SwiftFlutterPlugin : ObjectiveC.NSObject, Flutter.FlutterPlugin {
  public init(with registrar: Flutter.FlutterPluginRegistrar)
  @objc public static func register(with registrar: Flutter.FlutterPluginRegistrar)
  @objc deinit
  @objc override dynamic public init()
}
extension URLAuthenticationChallenge {
  public func toMap() -> [Swift.String : Any?]
}
extension URLCredential {
  public func toMap() -> [Swift.String : Any?]
}
extension URLProtectionSpace {
  public func toMap() -> [Swift.String : Any?]
}
extension URLRequest {
  public init(fromPluginMap: [Swift.String : Any?])
  public func toMap() -> [Swift.String : Any?]
}
extension URLResponse {
  public func toMap() -> [Swift.String : Any?]
}
@objc public class UserScript : WebKit.WKUserScript {
  @objc override dynamic public init(source: Swift.String, injectionTime: WebKit.WKUserScriptInjectionTime, forMainFrameOnly: Swift.Bool)
  public init(groupName: Swift.String?, source: Swift.String, injectionTime: WebKit.WKUserScriptInjectionTime, forMainFrameOnly: Swift.Bool)
  @available(iOS 14.0, *)
  @objc override dynamic public init(source: Swift.String, injectionTime: WebKit.WKUserScriptInjectionTime, forMainFrameOnly: Swift.Bool, in contentWorld: WebKit.WKContentWorld)
  @available(iOS 14.0, *)
  public init(groupName: Swift.String?, source: Swift.String, injectionTime: WebKit.WKUserScriptInjectionTime, forMainFrameOnly: Swift.Bool, in contentWorld: WebKit.WKContentWorld)
  public static func fromMap(map: [Swift.String : Any?]?, windowId: Swift.Int64?) -> flutter_inappwebview.UserScript?
  @objc override dynamic public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class Util {
  public static func getUrlAsset(assetFilePath: Swift.String) throws -> Foundation.URL
  public static func getAbsPathAsset(assetFilePath: Swift.String) throws -> Swift.String
  public static func convertToDictionary(text: Swift.String) -> [Swift.String : Any]?
  public static func JSONStringify(value: Any, prettyPrinted: Swift.Bool = false) -> Swift.String
  @available(iOS 14.0, *)
  public static func getContentWorld(name: Swift.String) -> WebKit.WKContentWorld
  @available(iOS 10.0, *)
  public static func getDataDetectorType(type: Swift.String) -> WebKit.WKDataDetectorTypes
  @available(iOS 10.0, *)
  public static func getDataDetectorTypeString(type: WebKit.WKDataDetectorTypes) -> [Swift.String]
  public static func getDecelerationRate(type: Swift.String) -> UIKit.UIScrollView.DecelerationRate
  public static func getDecelerationRateString(type: UIKit.UIScrollView.DecelerationRate) -> Swift.String
  public static func isIPv4(address: Swift.String) -> Swift.Bool
  public static func isIPv6(address: Swift.String) -> Swift.Bool
  public static func isIpAddress(address: Swift.String) -> Swift.Bool
  public static func normalizeIPv6(address: Swift.String) throws -> Swift.String
  @objc deinit
}
@objc public class WebMessage : ObjectiveC.NSObject {
  public init(data: Swift.String?, ports: [flutter_inappwebview.WebMessagePort]?)
  public func dispose()
  @objc deinit
  @objc override dynamic public init()
}
@objc public class WebMessageChannel : flutter_inappwebview.FlutterMethodCallDelegate {
  public init(id: Swift.String)
  public func initJsInstance(webView: flutter_inappwebview.InAppWebView, completionHandler: ((flutter_inappwebview.WebMessageChannel) -> Swift.Void)? = nil)
  override public func handle(_ call: Flutter.FlutterMethodCall, result: @escaping Flutter.FlutterResult)
  public func onMessage(index: Swift.Int64, message: Swift.String?)
  public func toMap() -> [Swift.String : Any?]
  public func dispose()
  @objc deinit
  @objc override dynamic public init()
}
@objc public class WebMessageListener : flutter_inappwebview.FlutterMethodCallDelegate {
  public init(jsObjectName: Swift.String, allowedOriginRules: Swift.Set<Swift.String>)
  public func assertOriginRulesValid() throws
  public func initJsInstance(webView: flutter_inappwebview.InAppWebView)
  public static func fromMap(map: [Swift.String : Any?]?) -> flutter_inappwebview.WebMessageListener?
  override public func handle(_ call: Flutter.FlutterMethodCall, result: @escaping Flutter.FlutterResult)
  public func isOriginAllowed(scheme: Swift.String?, host: Swift.String?, port: Swift.Int?) -> Swift.Bool
  public func onPostMessage(message: Swift.String?, sourceOrigin: Foundation.URL?, isMainFrame: Swift.Bool)
  public func dispose()
  @objc deinit
  @objc override dynamic public init()
}
@objc public class WebMessagePort : ObjectiveC.NSObject {
  public init(name: Swift.String, webMessageChannel: flutter_inappwebview.WebMessageChannel)
  public func setWebMessageCallback(completionHandler: ((Any?) -> Swift.Void)? = nil) throws
  public func postMessage(message: flutter_inappwebview.WebMessage, completionHandler: ((Any?) -> Swift.Void)? = nil) throws
  public func close(completionHandler: ((Any?) -> Swift.Void)? = nil) throws
  public func dispose()
  @objc deinit
  @objc override dynamic public init()
}
@objc @_hasMissingDesignatedInitializers public class WebViewTransport : ObjectiveC.NSObject {
  @objc override dynamic public init()
  @objc deinit
}
@available(iOS 14.0, *)
extension WKContentWorld {
  public static func fromMap(map: [Swift.String : Any?]?, windowId: Swift.Int64?) -> WebKit.WKContentWorld?
}
extension WKFrameInfo {
  public func toMap() -> [Swift.String : Any?]
}
extension WKNavigationAction {
  public func toMap() -> [Swift.String : Any?]
}
extension WKNavigationResponse {
  public func toMap() -> [Swift.String : Any?]
}
@available(iOS 9.0, *)
extension WKSecurityOrigin {
  public func toMap() -> [Swift.String : Any?]
}
extension WKUserContentController {
  public func initialize()
  public func dispose(windowId: Swift.Int64?)
  public func sync(scriptMessageHandler: WebKit.WKScriptMessageHandler)
  public func addUserOnlyScript(_ userOnlyScript: flutter_inappwebview.UserScript)
  public func addUserOnlyScripts(_ userOnlyScripts: [flutter_inappwebview.UserScript])
  public func addPluginScript(_ pluginScript: flutter_inappwebview.PluginScript)
  public func addPluginScripts(_ pluginScripts: [flutter_inappwebview.PluginScript])
  public func getPluginScriptsRequiredInAllContentWorlds() -> [flutter_inappwebview.PluginScript]
  @available(iOS 14.0, *)
  public func generateCodeForScriptEvaluation(scriptMessageHandler: WebKit.WKScriptMessageHandler, source: Swift.String, contentWorld: WebKit.WKContentWorld) -> Swift.String
  public func removeUserOnlyScript(_ userOnlyScript: flutter_inappwebview.UserScript)
  public func removeUserOnlyScript(at index: Swift.Int, injectionTime: WebKit.WKUserScriptInjectionTime)
  public func removeAllUserOnlyScripts()
  public func removePluginScript(_ pluginScript: flutter_inappwebview.PluginScript)
  public func removeAllPluginScripts()
  public func removeAllPluginScriptMessageHandlers()
  @available(iOS 14.0, *)
  public func resetContentWorlds(windowId: Swift.Int64?)
  public func removeUserOnlyScripts(with groupName: Swift.String, shouldAddPreviousScripts: Swift.Bool = true)
  public func removePluginScripts(with groupName: Swift.String, shouldAddPreviousScripts: Swift.Bool = true)
  public func containsPluginScript(with groupName: Swift.String) -> Swift.Bool
  @available(iOS 14.0, *)
  public func containsPluginScript(with groupName: Swift.String, in contentWorld: WebKit.WKContentWorld) -> Swift.Bool
  @available(iOS 14.0, *)
  public func getContentWorlds(with windowId: Swift.Int64?) -> Swift.Set<WebKit.WKContentWorld>
}
extension WKWindowFeatures {
  public func toMap() -> [Swift.String : Any?]
}
extension flutter_inappwebview.HitTestResultType : Swift.Equatable {}
extension flutter_inappwebview.HitTestResultType : Swift.Hashable {}
extension flutter_inappwebview.HitTestResultType : Swift.RawRepresentable {}
